/* Options to flex */
%option outfile="scanner.c"
%option noyywrap
%option yylineno
%option noinput
%option nounput

%{ 
#include "ast.h"
#include "parser.h"
#include "tables.h"
extern LitTable* litTable;
char idText[128];
%}

%x COMMENT LINECOMMENT

%%
else	{ return ELSE; }
if	{ return IF; }
input	{ return INPUT; }
int	{ return INT; }
output	{ return OUTPUT; }
return	{ return RETURN; }
void	{ return VOID; }
while	{ return WHILE; }
write	{ return WRITE; }

"+"	{ return PLUS; }
"-"	{ return MINUS; }
"*"	{ return TIMES; }
"/"	{ return OVER; }
"<"	{ return LT; }
"<="	{ return LE; }
">"	{ return GT; }
">="	{ return GE; }
"=="	{ return EQ; }
"!="	{ return NEQ; }
"="	{ return ASSIGN; }

";"	{ return SEMI; }
","	{ return COMMA; }
"("	{ return LPAREN; }
")"	{ return RPAREN; }
"["	{ return LBRACK; }
"]"	{ return RBRACK; }
"{"	{ return LBRACE; }
"}"	{ return RBRACE; }

\"[^"]*\"	{ 
    LitTableEntry* litEntry = LT_add_literal(litTable, yytext);
    yylval = AST_new_leaf(STRING_NODE, 0, litEntry); 
    return STRING; 
  }

"//" 			BEGIN(LINECOMMENT);
<LINECOMMENT>"\n"	BEGIN(INITIAL);
<LINECOMMENT>.		;

"/*"		BEGIN(COMMENT);
<COMMENT>"*/"	BEGIN(INITIAL);
<COMMENT>.|\n	;

[ \t\n]+		;
[0-9]+			{ yylval = AST_new_leaf(NUM_NODE, atoi(yytext), NULL); return NUM; }
[a-zA-Z][a-zA-Z0-9]*	{ strcpy(idText, yytext); return ID; }

.		{ printf("SCANNING ERROR (%d): Unknown symbol %s\n", yylineno, yytext);
		  exit(1);}

%%
