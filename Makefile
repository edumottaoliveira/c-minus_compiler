
all: bison flex gcc
	@echo "Done."

bison: parser.y
	bison parser.y

flex: scanner.l
	flex scanner.l

gcc: scanner.c parser.c ast.c tables.c interpreter.c stack.c list.c frame.c
	gcc -Wall -g -o trab4 scanner.c parser.c ast.c tables.c interpreter.c stack.c list.c frame.c -ly

clean:
	@rm -f *.o *.output scanner.c parser.h parser.c trab4 *.dot *.pdf
