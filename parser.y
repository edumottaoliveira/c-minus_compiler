%output "parser.c"
%defines "parser.h"
%define parse.error verbose
%define parse.lac full
%define parse.trace

%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ast.h"
#include "tables.h"
#include "interpreter.h"

int yylex(void);
void yyerror(char const *s);
SymTableEntry* reg_var();
SymTableEntry* check_var();
SymTableEntry* reg_func(char* funcId, int arity);
SymTableEntry* check_func(char* funcId, int arity);
void exit_with_error();

extern int yylineno;
extern char idText[128];

AST* tree;
LitTable* litTable;
SymTable* varTable;
SymTable* funcTable;
int scope = 0;
char tmpFuncId[128];
SymTableEntry* tmpEntry = NULL;

%}

%define api.value.type {AST*} // Type of variable yylval;

%token IF ELSE INPUT INT OUTPUT RETURN VOID WHILE WRITE
%token LE GT GE NEQ ASSIGN
%token SEMI COMMA LPAREN RPAREN LBRACK RBRACK LBRACE RBRACE
%token STRING NUM ID UNKNOWN

%left EQ LT
%left PLUS MINUS
%left TIMES OVER  
%%

program: 
	func_decl_list { tree = $1;}
;
func_decl_list: 
	func_decl_list func_decl { AST_add_child($1, $2); $$ = $1;}
	| func_decl { $$ = AST_new_node(FUNC_LIST_NODE, 1, $1);}
;
func_decl: 
	func_header func_body { 
		scope++; // Increase after end of function
		$$ = AST_new_node(FUNC_DECL_NODE, 2, $1, $2); }
;
func_header: 
	ret_type ID {strcpy(tmpFuncId, idText);} LPAREN params RPAREN {
		SymTableEntry* entry = reg_func(tmpFuncId,AST_count_children($5));
		AST* func_name_node = AST_new_leaf(FUNC_NAME_NODE, 0, NULL);
		AST* func_header_node = AST_new_node(FUNC_HEADER_NODE, 2, func_name_node, $5);
		AST_set_table_entry(func_header_node, entry);
		$$ = func_header_node; } 
;

func_body: 
	LBRACE opt_var_decl opt_stmt_list RBRACE 	{ 
		$$ = AST_new_node(FUNC_BODY_NODE, 2, $2, $3);}
;
opt_var_decl:
	%empty  	{ $$ = AST_new_node(VAR_LIST_NODE, 0); }
	| var_decl_list { $$ = $1; }
;
opt_stmt_list:
	%empty		{ $$ = AST_new_node(BLOCK_NODE, 0); }
	| stmt_list { $$ = $1; }
;

ret_type: INT 
		| VOID
;
params: 
	VOID 			{ $$ = AST_new_leaf(PARAM_LIST_NODE, 0, NULL);}
	| param_list	{ $$ = $1;}
;
param_list: 
	param_list COMMA param 	{ AST_add_child($1, $3); $$ = $1;}
	| param  				{ $$ = AST_new_node(PARAM_LIST_NODE, 1, $1);}
;
param: 
	INT ID 					{ $$ = AST_new_leaf(VAR_DECL_NODE, 0, (void*)reg_var());}
	| INT ID LBRACK RBRACK 	{ $$ = AST_new_leaf(VAR_DECL_NODE, 0, (void*)reg_var());}
;

var_decl_list: 
	var_decl_list var_decl	{ AST_add_child($1, $2); $$ = $1;}
	| var_decl 				{ $$ = AST_new_node(VAR_LIST_NODE, 1, $1);}
;
var_decl: 
	INT ID SEMI 					{ $$ = AST_new_leaf(VAR_DECL_NODE, 0, (void*)reg_var());}
	| INT ID LBRACK NUM RBRACK SEMI { $$ = AST_new_leaf(VAR_DECL_NODE, 0, (void*)reg_var());}
;
stmt_list: 
	stmt_list stmt 	{ AST_add_child($1, $2); $$ = $1;}
	| stmt			{ $$ = AST_new_node(BLOCK_NODE, 1, $1);}
;
stmt: assign_stmt 		{ $$ = $1;}
	| if_stmt 			{ $$ = $1;}
	| while_stmt 		{ $$ = $1;}
	| return_stmt 		{ $$ = $1;}
	| func_call SEMI	{ $$ = $1;}
;
assign_stmt: 
	lval ASSIGN arith_expr SEMI { $$ = AST_new_node(ASSIGN_NODE, 2, $1, $3);}
;
lval: 
	ID { tmpEntry = check_var();} lval_expanded { $$ = $3;}
;
lval_expanded:
	%empty				{ $$ = AST_new_leaf(VAR_USE_NODE, 0, tmpEntry);}
	| LBRACK NUM RBRACK { free($2); $$ = AST_new_leaf(VAR_USE_NODE, 0, tmpEntry);}
	| LBRACK ID RBRACK 	{ AST* idNode = AST_new_leaf(VAR_USE_NODE, 0, NULL);
							$$ = AST_new_node(VAR_USE_NODE, 1, idNode);}
;
if_stmt: 
	IF LPAREN bool_expr RPAREN block {	$$ = AST_new_node(IF_NODE, 2, $3, $5);}
   | IF LPAREN bool_expr RPAREN block ELSE block {	
	   $$ = AST_new_node(IF_NODE, 3, $3, $5, $7);}
;
block: 
	LBRACE stmt_list RBRACE 	{ $$ = $2;}
	| LBRACE RBRACE 		   	{ $$ = AST_new_node(BLOCK_NODE, 0);}
;
while_stmt: 
	WHILE LPAREN bool_expr RPAREN block {	
			AST* n = AST_new_node(WHILE_NODE, 1, $3); 
			AST_add_child(n, $5);
			$$ = n;
		}
;
return_stmt: 
	RETURN SEMI { $$ = AST_new_node(RETURN_NODE, 0);}
	| RETURN arith_expr SEMI { $$ = AST_new_node(RETURN_NODE, 1, $2);}
;
func_call: output_call 		{ $$ = $1;}
		 | write_call 		{ $$ = $1;}
		 | user_func_call	{ $$ = $1;}
;
input_call: 
	INPUT LPAREN RPAREN { $$ = AST_new_node(INPUT_NODE, 0);}
;
output_call: 
	OUTPUT LPAREN arith_expr RPAREN { $$ = AST_new_node(OUTPUT_NODE, 1, $3);}
;
write_call: 
	WRITE LPAREN STRING RPAREN { $$ = AST_new_node(WRITE_NODE, 1, $3);}
;
user_func_call: 
	ID {strcpy(tmpFuncId, idText); } LPAREN opt_arg_list RPAREN {
		SymTableEntry* table_entry = check_func(tmpFuncId, AST_count_children($4));
		AST* fcall_node = AST_new_node(FCALL_NODE, 1, $4);
		AST_set_table_entry(fcall_node, table_entry);
		$$ = fcall_node;}
;
opt_arg_list:
	%empty     { $$ = AST_new_node(ARG_LIST_NODE, 0);}
	| arg_list { $$ = $1;}
;
arg_list: 
	arg_list COMMA arith_expr { AST_add_child($1, $3); $$ = $1;}
	| arith_expr { $$ = AST_new_node(ARG_LIST_NODE, 1, $1);}
;
bool_expr: 
	arith_expr LT arith_expr 	{ $$ = AST_new_node(BOOL_LT_NODE, 2, $1, $3);}
	| arith_expr LE arith_expr	{ $$ = AST_new_node(BOOL_LE_NODE, 2, $1, $3);}
	| arith_expr GT arith_expr	{ $$ = AST_new_node(BOOL_GT_NODE, 2, $1, $3);}
	| arith_expr GE arith_expr	{ $$ = AST_new_node(BOOL_GE_NODE, 2, $1, $3);}
	| arith_expr EQ arith_expr	{ $$ = AST_new_node(BOOL_EQ_NODE, 2, $1, $3);}
	| arith_expr NEQ arith_expr	{ $$ = AST_new_node(BOOL_NEQ_NODE, 2, $1, $3);}
;
arith_expr: 
	arith_expr PLUS arith_expr		{ $$ = AST_new_node(PLUS_NODE, 2, $1, $3);}
	| arith_expr MINUS arith_expr	{ $$ = AST_new_node(MINUS_NODE, 2, $1, $3);}
	| arith_expr TIMES arith_expr	{ $$ = AST_new_node(TIMES_NODE, 2, $1, $3);}
	| arith_expr OVER arith_expr	{ $$ = AST_new_node(OVER_NODE, 2, $1, $3);}
	| LPAREN arith_expr RPAREN 		{ $$ = $2;}
	| lval 							{ $$ = $1;}
	| input_call 					{ $$ = $1;}
	| user_func_call 				{ $$ = $1;}
	| NUM 							{ $$ = $1;}
;

%%
int main() {
	litTable = LT_create_lit_table();
	varTable =  ST_create_sym_table();
	funcTable =  ST_create_sym_table();

	int result = yyparse();
	if (result == 0) {
		//printf("PARSE SUCCESSFUL!\n");
		//AST_print_dot(tree);
		stdin = fopen(ctermid(NULL), "r");
		run_ast(tree);
        AST_free_tree(tree);
	}

	//print_lit_table(litTable);
	//print_var_table(varTable);
	//print_func_table(funcTable);

	LT_free_lit_table(litTable);
	ST_free_sym_table(varTable);
	ST_free_sym_table(funcTable);
}

// Error handling.
void yyerror (char const *s) {
	printf("PARSE ERROR (%d): %s\n", yylineno, s);
}

SymTableEntry* reg_var() {
    SymTableEntry* entry = ST_lookup_var(varTable, idText, scope);
    if (entry != NULL) {
        printf("SEMANTIC ERROR (%d): variable '%s' already declared at line %d.\n",
                yylineno, idText, ST_get_line(entry));
        exit_with_error();
    }
    return ST_add_var(varTable, idText, yylineno, scope);
}

SymTableEntry* check_var() {
    SymTableEntry* entry = ST_lookup_var(varTable, idText, scope);
    if (entry == NULL) {
        printf("SEMANTIC ERROR (%d): variable '%s' was not declared.\n",
                yylineno, idText);
        exit_with_error();
    } 
	return entry;
}

SymTableEntry* reg_func(char* funcId, int arity) {
    SymTableEntry* entry = ST_lookup_func(funcTable, funcId, arity);
    if (entry != NULL) {
        printf("SEMANTIC ERROR (%d): function '%s' already declared at line %d.\n",
                yylineno, funcId, ST_get_line(entry));
        exit_with_error();
    }
    return ST_add_func(funcTable, funcId, yylineno, arity);
}

SymTableEntry* check_func(char* funcId, int arity) {
    SymTableEntry* entry = ST_lookup_func(funcTable, funcId, arity);
    if (entry == NULL) {
        printf("SEMANTIC ERROR (%d): function '%s' was not declared.\n",
                yylineno, funcId);
        exit_with_error();
    }
	else if (ST_get_arity(entry) != arity) {
		printf("SEMANTIC ERROR (%d): function '%s' was called with %d arguments but declared with %d parameters.\n",
                yylineno, funcId, arity, ST_get_arity(entry));
		exit_with_error();
	}
	return entry;
}

void exit_with_error() {
	//print_lit_table(litTable);
	//print_var_table(varTable);
	//print_func_table(funcTable);
	
	AST_free_tree(tree);
	ST_free_sym_table(varTable);
	ST_free_sym_table(funcTable);
	LT_free_lit_table(litTable);
	exit(1);
}
