
#ifndef TABLES_H
#define TABLES_H

// Literals Table
// ----------------------------------------------------------------------------

// Opaque structure.
struct lit_table;
typedef struct lit_table LitTable;
typedef struct lit_table_entry LitTableEntry;

// Creates an empty literal table.
LitTable* LT_create_lit_table();

// Adds the given string to the table without repetitions.
// String 's' is copied internally.
LitTableEntry* LT_add_literal(LitTable* lt, char* s);

// Returns a pointer to the string stored at a entry lte
char* LT_get_literal(LitTableEntry* lte);

// Prints the given table to stdout.
void LT_print_lit_table(LitTable* lt);

// Clears the allocated structure.
void LT_free_lit_table(LitTable* lt);


// Symbols Table
// ----------------------------------------------------------------------------

// Opaque structure.
struct sym_table;
typedef struct sym_table SymTable;

//struct entry_sym_table;
typedef struct entry_sym_table SymTableEntry;

// Creates an empty symbol table.
SymTable* ST_create_sym_table();

// Adds a fresh var to the table.
// No check is made by this function, so make sure to call 'lookup_var' first.
SymTableEntry* ST_add_var(SymTable* st, char* s, int line, int scope);
SymTableEntry* ST_add_func(SymTable* st, char* s, int line, int arity);

// Returns the pointer to a SymTableEntry where the given variable is stored or NULL otherwise.
SymTableEntry* ST_lookup_var(SymTable* st, char* s, int scope);
SymTableEntry* ST_lookup_func(SymTable* st, char* s, int arity);

// Returns variables stored at the given entry.
// No check is made by this function, so make sure that the pointer is valid first.
char* ST_get_name(SymTableEntry* entry);
int ST_get_line(SymTableEntry* entry);
int ST_get_arity(SymTableEntry* entry);
int ST_get_scope(SymTableEntry* entry);
int ST_get_offset(SymTableEntry* entry);
void ST_set_offset(SymTableEntry* entry, int offset);

// Prints the given table to stdout.
void ST_print_var_table_entry(SymTableEntry* entry);
void ST_print_var_table(SymTable* st);
void ST_print_func_table(SymTable* st);

// Clears a linked list of SymTableEntry's
void ST_free_entries(SymTableEntry* firstEntry);

// Clears the allocated structure.
void ST_free_sym_table(SymTable* st);

#endif // TABLES_H

