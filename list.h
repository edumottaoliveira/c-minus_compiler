#ifndef LIST_H
#define LIST_H

struct list;
typedef struct list List;

List* List_create();

int List_appendInt(int value, List** ls);

int List_appendArray(int* array, List** ls);

List* List_getCell(int index, List* ls);

int List_isArrayCell(List* cell);

int List_isIntCell(List* cell);

int List_getValueCell(List* cell);

int* List_getArrayCell(List* cell);

void List_setValueCell(int value, List* cell);

void List_free(List* ls);

void List_print(List* ls);

#endif
