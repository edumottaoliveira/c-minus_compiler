#ifndef FRAME_H
#define FRAME_H

struct frame;
typedef struct frame Frame;

Frame* Frame_create(List* local, Stack* stack);

void Frame_pushFrame(Frame* f);

void Frame_popFrame();

void Frame_free(Frame* f);

int Frame_appendLocalInt(int value, Frame* f);

int Frame_appendLocalArray(int* array, Frame* f);

int Frame_getValueLocalInt(int offset, Frame* f);

void Frame_setValueLocalInt(int value, int offset, Frame* f);

void Frame_stackPushNum(int value, Frame* f);

void Frame_stackPushLocal(int offset, Frame* f);

int Frame_stackPop(Frame* f);

int Frame_isStackEmpty(Frame* f);

void Frame_print(Frame *f);

#endif
