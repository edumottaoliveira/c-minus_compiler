#ifndef INTERPRETER_H
#define INTERPRETER_H

#include "ast.h"

void run_ast(AST* tree);
void rec_ast_run(AST* node);
void run_id_node(AST* node);
void run_num_node(AST* node);
void run_func_list_node(AST* node);
void run_func_decl_node(AST* node);
void run_func_header_node(AST* node);
void run_func_body_node(AST* node);
void run_param_list_node(AST* node);
void run_var_list_node(AST* node);
void run_block_node(AST* node);
void run_assign_node(AST* node);
void run_if_node(AST* node);
void run_while_node(AST* node);
void run_return_node(AST* node);
void run_input_node(AST* node);
void run_output_node(AST* node);
void run_write_node(AST* node);
void run_fcall_node(AST* node);
void run_arg_list_node(AST* node);
void run_bool_lt_node(AST* node);
void run_bool_le_node(AST* node);
void run_bool_gt_node(AST* node);
void run_bool_ge_node(AST* node);
void run_bool_eq_node(AST* node);
void run_bool_neq_node(AST* node);
void run_plus_node(AST* node);
void run_minus_node(AST* node);
void run_times_node(AST* node);
void run_over_node(AST* node);
void run_func_name_node(AST* node);
void run_string_node(AST* node);
void run_var_decl_node(AST* node);
void run_var_use_node(AST* node); 

AST* lookup_func_node(SymTableEntry* table_entry, AST* func_list_node); 

#endif
