
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "tables.h"
#include "ast.h"

struct node {
	NodeKind kind;
	int data; 
	void* tableEntry; //LitTableEntry* or SymTableEntry*
	AST* child;
	AST* next_child;
};

AST* AST_new_leaf(NodeKind kind, int data, void* tableEntry) {
	AST *leaf = AST_new_node(kind, 0);
	leaf->data = data;
	leaf->tableEntry = tableEntry;
	//AST_print_node_info(leaf);
	//ST_print_var_table_entry((SymTableEntry*)AST_get_table_entry(leaf));
	//printf("data: %d\n", data);
    return leaf;
}

AST* AST_new_node(NodeKind kind, int numChildren, ...) {
	AST* node = malloc(sizeof * node);
	node->kind = kind;
	node->data = 0; // Nao usado
	node->tableEntry = NULL;
	node->child = NULL;
	node->next_child = NULL;

	va_list arguments;
	va_start(arguments, numChildren);

	if (numChildren == 0) {
		return node;
	}

	node->child = va_arg(arguments, AST*);
	int i;

	AST* current = node->child;
	for (i = 1; i < numChildren; i++) {
		current->next_child = va_arg(arguments, AST*);
		current = current->next_child;
	}
	current->next_child = NULL;

	return node;
}

void AST_add_child(AST* parent, AST* child) {
	if (child == NULL || parent == NULL) {
		return;
	}
	child->next_child = NULL;	

	if (parent->child == NULL) {
		parent->child = child;
	}
	else {
		AST* current = parent->child;
		while (current->next_child != NULL) {
			current = current->next_child;
		}
		current->next_child = child;
	}
}

int AST_count_children(AST* parent) {
	if (parent->child == NULL) {
		return 0;
	}
	else {
		int count = 1;
		AST* current = parent->child;
		while (current->next_child != NULL) {
			count++;
			current = current->next_child;
		}
		return count;
	}
}

NodeKind AST_get_kind(AST* node) {
	return node->kind;
}

void AST_set_table_entry(AST* node, void* tableEntry) {
	if (node == NULL) {
		printf("Error: AST_set_table_entry() with null node\n");
		exit(1);
	}
	node->tableEntry = tableEntry;
}

void* AST_get_table_entry(AST* node) {
	return node->tableEntry;
}

int AST_get_data(AST* node) {
	return node->data;
}

AST* AST_get_child(AST* node, int index) {
	if (node->child == NULL) {
		printf("Error: get_child(AST*, int) called in node with no child\n");
		exit(1);
	}
	if (index == 0) {
		return node->child;
	}
	int i = 0;
	AST* current = node->child;
	while (current->next_child != NULL) {
		current = current->next_child;
		i++;
		if (i == index) {
			return current;
		}
	}
	printf("Error: get_child(AST*, int) with index out of range\n");
	exit(1);
}

void AST_free_tree(AST* tree) {
	if (tree != NULL) {
		AST* current = tree->child;
		AST* tmp;

		while (current != NULL) {
			tmp = current;
			current = current->next_child;
			AST_free_tree(tmp);
		}

		if (current != NULL) {
			AST_free_tree(current);
		}

		free(tree);
	}
}

// Dot output.

void AST_node2str(AST *node, char *s) {
    switch(node->kind) {
        case NUM_NODE: 			sprintf(s, "num,%d", node->data); 	break;
		case ID_NODE:  			sprintf(s, "id"); 	break;
		case FUNC_LIST_NODE: 	sprintf(s, "func_list"); 		break;
		case FUNC_DECL_NODE: 	sprintf(s, "func_decl");		break;
		case FUNC_HEADER_NODE: 	sprintf(s, "func_header"); 	break;
		case FUNC_BODY_NODE: 	sprintf(s, "func_body"); 		break;
		case PARAM_LIST_NODE: 	sprintf(s, "param_list"); 	break;
		case VAR_LIST_NODE: 	sprintf(s, "var_list"); 		break;
		case BLOCK_NODE: 		sprintf(s, "block"); 			break;
		case ASSIGN_NODE: 		sprintf(s, "="); 				break;
		case IF_NODE:			sprintf(s, "if");				break;
		case WHILE_NODE:		sprintf(s, "while");			break;
		case RETURN_NODE: 		sprintf(s, "return");			break;
		case INPUT_NODE: 		sprintf(s, "input");			break;
		case OUTPUT_NODE:		sprintf(s, "output");			break;
		case WRITE_NODE: 		sprintf(s, "write");			break;
		case FCALL_NODE: 		sprintf(s, "fcall");			break;
		case ARG_LIST_NODE: 	sprintf(s, "arg_list");		break;
		case BOOL_LT_NODE:		sprintf(s, "<");		break;
		case BOOL_LE_NODE:		sprintf(s, "<=");		break;
		case BOOL_GT_NODE:		sprintf(s, ">");		break;
		case BOOL_GE_NODE:		sprintf(s, ">=");		break;
		case BOOL_EQ_NODE:		sprintf(s, "==");		break;
		case BOOL_NEQ_NODE:		sprintf(s, "!=");		break;
		case PLUS_NODE:			sprintf(s, "+");		break;
		case MINUS_NODE:		sprintf(s, "-");		break;
		case TIMES_NODE:		sprintf(s, "*");		break;
		case OVER_NODE:			sprintf(s, "/");		break;
		case FUNC_NAME_NODE:	sprintf(s, "func_name");	break;
		case STRING_NODE:		sprintf(s, "string");	break;
		case VAR_DECL_NODE: 	sprintf(s, "var_decl");	break;
		case VAR_USE_NODE: 		sprintf(s, "var_use");	break;
		default: printf("Invalid node kind: %d!\n", node->kind);
    }
}

void AST_print_node_info(AST* node) {
	char kindStr[10];
	AST_node2str(node, kindStr);	
	printf("NODE -- kind: %s, data: %d, tableEntry: %s, children: %d\n",
		kindStr, node->data, node->tableEntry == NULL ? "null" : "not null", AST_count_children(node) );
}


int nr;

int AST_print_node_dot(AST* node) {
	char s[10];
	int my_nr = nr++;
	AST_node2str(node, s);
    printf("node%d[label=\"%s\"];\n", my_nr, s);

	AST* current = node->child;
	while (current != NULL) {
		int child_nr = AST_print_node_dot(current);
        printf("node%d -> node%d;\n", my_nr, child_nr);
		current = current->next_child;
	}
    return my_nr;
}

void AST_print_dot(AST* tree) {
    nr = 0;
    printf("digraph {\ngraph [ordering=\"out\"];\n");
    AST_print_node_dot(tree);
    printf("}\n");
}
