
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ast.h"
#include "tables.h"
#include "stack.h"
#include "list.h"
#include "frame.h"
#include "interpreter.h"


extern LitTable* litTable;
extern SymTable* varTable;
extern SymTable* funcTable;
extern Frame* frame_ptr;
AST* func_list_ptr = NULL;

void rec_ast_run(AST* node) {
    switch(AST_get_kind(node)) {
        case ID_NODE:           run_id_node(node);       break;
        case NUM_NODE:          run_num_node(node);       break;
        case FUNC_LIST_NODE:    run_func_list_node(node);       break;
        case FUNC_DECL_NODE:    run_func_decl_node(node);       break;
        case FUNC_HEADER_NODE:  run_func_header_node(node);       break;
        case FUNC_BODY_NODE:    run_func_body_node(node);       break;
        case PARAM_LIST_NODE:   run_param_list_node(node);       break;
        case VAR_LIST_NODE:     run_var_list_node(node);       break;
        case BLOCK_NODE:        run_block_node(node);       break;
        case ASSIGN_NODE:       run_assign_node(node);       break;
        case IF_NODE:           run_if_node(node);       break;
        case WHILE_NODE:        run_while_node(node);       break;
        case RETURN_NODE:       run_return_node(node);       break;
        case INPUT_NODE:        run_input_node(node);       break;
        case OUTPUT_NODE:       run_output_node(node);       break;
        case WRITE_NODE:        run_write_node(node);       break;
        case FCALL_NODE:        run_fcall_node(node);       break;
        case ARG_LIST_NODE:     run_arg_list_node(node);       break;
        case BOOL_LT_NODE:      run_bool_lt_node(node);       break;
        case BOOL_LE_NODE:      run_bool_le_node(node);       break;
        case BOOL_GT_NODE:      run_bool_gt_node(node);       break;
        case BOOL_GE_NODE:      run_bool_ge_node(node);       break;
        case BOOL_EQ_NODE:      run_bool_eq_node(node);       break;
        case BOOL_NEQ_NODE:     run_bool_neq_node(node);       break;
        case PLUS_NODE:         run_plus_node(node);       break;
        case MINUS_NODE:        run_minus_node(node);       break;
        case TIMES_NODE:        run_times_node(node);       break;
        case OVER_NODE:         run_over_node(node);       break;
        case FUNC_NAME_NODE:    run_func_name_node(node);       break;
        case STRING_NODE:       run_string_node(node);       break;
        case VAR_DECL_NODE:     run_var_decl_node(node);       break;
        case VAR_USE_NODE:      run_var_use_node(node);       break;
    }
}

#define bin_op() \
    rec_ast_run(AST_get_child(node,0)); \
    rec_ast_run(AST_get_child(node,1)); \
    int r = Frame_stackPop(frame_ptr); \
    int l = Frame_stackPop(frame_ptr);

void run_ast(AST* tree) {
    //printf("Root: %p\n", tree);
    rec_ast_run(tree); 
    Frame_popFrame(); 
}

void run_id_node(AST* node) {
}

void run_num_node(AST* node) {
    Frame_stackPushNum(AST_get_data(node), frame_ptr);
}

void run_func_list_node(AST* node) {
    func_list_ptr = node; // Armazena para buscar fucoes mais tarde
    int count = AST_count_children(node);
    rec_ast_run(AST_get_child(node, count-1)); // run Main func.
}

void run_func_decl_node(AST* node) {
    Frame_pushFrame(Frame_create(NULL, NULL));
    int i, children = AST_count_children(node);
    for (i = 0; i < children; i++) {
        rec_ast_run(AST_get_child(node, i));
    }
}

void run_func_header_node(AST* node) {
    //printf("header\n");
    //rec_ast_run(AST_get_child(node, 0));
    //rec_ast_run(AST_get_child(node, 1));
}

void run_func_body_node(AST* node) {
    //printf("body\n");
    rec_ast_run(AST_get_child(node, 0)); // var_list
    rec_ast_run(AST_get_child(node, 1)); // block
}

void run_param_list_node(AST* node) {
}

void run_var_list_node(AST* node) {
    int i, var_count = AST_count_children(node);
    for (i = 0; i < var_count; i++) {
        AST* var_node = AST_get_child(node, i);
        rec_ast_run(var_node);
    }
}

void run_block_node(AST* node) {
    //printf("block\n");
    int i, children = AST_count_children(node);
    for (i = 0; i < children; i++) {
        rec_ast_run(AST_get_child(node, i));
    }
}

void run_assign_node(AST* node) {
    
    rec_ast_run(AST_get_child(node,1));
    AST* id_node = AST_get_child(node,0);

    SymTableEntry* id_entry = AST_get_table_entry(id_node);
    int offset = ST_get_offset(id_entry);

    int value = Frame_stackPop(frame_ptr);
    Frame_setValueLocalInt(value, offset, frame_ptr);
    //int offset = Frame_appendLocalInt(value, frame_ptr);
   // ST_set_offset(id_entry, offset);


}

void run_if_node(AST* node) {
    rec_ast_run(AST_get_child(node,0));
    if (Frame_stackPop(frame_ptr)) {
        rec_ast_run(AST_get_child(node,1));
    }
    else if (AST_count_children(node) == 3) {
            rec_ast_run(AST_get_child(node,2));
    }
}

void run_while_node(AST* node) {
    rec_ast_run(AST_get_child(node,0));
    while (Frame_stackPop(frame_ptr) == 1) {
        rec_ast_run(AST_get_child(node,1));
        rec_ast_run(AST_get_child(node,0));
    } 
}

void run_return_node(AST* node) {

    int count = AST_count_children(node);
    if (count == 0) {
        Frame_popFrame();
        return;
    }

    AST* returned_var_node = AST_get_child(node, 0);
    rec_ast_run(returned_var_node);
    int returned_value = Frame_stackPop(frame_ptr);
    Frame_popFrame();
    Frame_stackPushNum(returned_value, frame_ptr);

    //NodeKind kind = AST_get_kind(returned_var_node); 

    /*SymTableEntry* var_entry = AST_get_table_entry(returned_var_node);
    int offset = ST_get_offset(var_entry);
    int returned_value = Frame_getValueLocalInt(offset, frame_ptr);
    Frame_popFrame();
    Frame_stackPushNum(returned_value, frame_ptr);*/
}

void run_input_node(AST* node) {
    int value;
    printf("input: ");
    scanf("%d", &value);
    Frame_stackPushNum(value, frame_ptr);
}

void run_output_node(AST* node) {
    rec_ast_run(AST_get_child(node,0));
    printf("%d",Frame_stackPop(frame_ptr));
}

void run_write_node(AST* node) {
    LitTableEntry* lit_entry = (LitTableEntry*) AST_get_table_entry(AST_get_child(node, 0));
    char* str = LT_get_literal(lit_entry);
    //str[strcspn(str,"\\n")] = '\n';
    int i, size = strlen(str);
    for (i = 0; i < size; i++) {
        if (str[i] == '\\') {
            if (i < size && str[i+1] == 'n') {
                printf("\n");
                i++;
            }
        }
        else if (str[i] != '"') {
            putc(str[i], stdout);
        }
    }
}
void run_fcall_node(AST* node) {

    // Chamada da funcao
    SymTableEntry* func_entry = AST_get_table_entry(node);
    AST* args_node = AST_get_child(node, 0);
    int i, num_args = AST_count_children(args_node);

    // Funcao
    AST* func_node = lookup_func_node(func_entry, func_list_ptr);
    AST* func_header_node = AST_get_child(func_node, 0);
    AST* param_list_node = AST_get_child(func_header_node, 1);

    List* list = List_create();
    for (i = 0; i < num_args; i++) { 
        // Copia argumentos da chamada para frame da funcao
        rec_ast_run(AST_get_child(args_node, i));
        int arg = Frame_stackPop(frame_ptr);
        List_appendInt(arg, &list);

        // Ajusta os offsets da variaveis locais
        AST* arg_node_func = AST_get_child(param_list_node, i);
        SymTableEntry* arg_table_entry = AST_get_table_entry(arg_node_func);
        ST_set_offset(arg_table_entry, i);
    }
    Frame_pushFrame(Frame_create(list, NULL));

    
    AST* func_body_node = AST_get_child(func_node, 1); // Body
    rec_ast_run(func_body_node);
}

void run_arg_list_node(AST* node) {
}

void run_bool_lt_node(AST* node) {
    bin_op();
    Frame_stackPushNum(l < r, frame_ptr);
}

void run_bool_le_node(AST* node) {
    bin_op();
    Frame_stackPushNum(l <= r, frame_ptr);
}

void run_bool_gt_node(AST* node) {
    bin_op();
    Frame_stackPushNum(l > r, frame_ptr);
}

void run_bool_ge_node(AST* node) {
    bin_op();
    Frame_stackPushNum(l >= r, frame_ptr);
}
void run_bool_eq_node(AST* node) {
    bin_op();
    Frame_stackPushNum(l == r, frame_ptr);
}
void run_bool_neq_node(AST* node) {
    bin_op();
    Frame_stackPushNum(l != r, frame_ptr);
}

void run_plus_node(AST* node) {
    bin_op();
    Frame_stackPushNum(l + r, frame_ptr);
}

void run_minus_node(AST* node) {
    bin_op();
    Frame_stackPushNum(l - r, frame_ptr);
}

void run_times_node(AST* node) {
    bin_op();
    Frame_stackPushNum(l * r, frame_ptr);
}
void run_over_node(AST* node) {
    bin_op();
    Frame_stackPushNum(l / r, frame_ptr);
}

void run_func_name_node(AST* node) {

}

void run_string_node(AST* node) {
}

void run_var_decl_node(AST* node) {
    SymTableEntry* entry = AST_get_table_entry(node);
    int offset = Frame_appendLocalInt(-100, frame_ptr); // Qualquer valor inicial
    ST_set_offset(entry, offset);
   
}
void run_var_use_node(AST* node) {
    SymTableEntry* entry = AST_get_table_entry(node);
    Frame_stackPushLocal(ST_get_offset(entry), frame_ptr);
}

AST* lookup_func_node(SymTableEntry* table_entry, AST* func_list_node) {
    if (func_list_node == NULL) {
        printf("Error: lookup_func_node() with null func_list_node\n");
        exit(1);
    }
    if (table_entry == NULL) {
        printf("Error: lookup_func_node() with null table_entry\n");
        exit(1);
    }
    int i, num_funcs = AST_count_children(func_list_node);
    num_funcs--; // nao contar main()
    for (i = 0; i < num_funcs; i++) {
        
        AST* func_node = AST_get_child(func_list_node, i);
        AST* header_func_node = AST_get_child(func_node, 0);

        SymTableEntry* entry = AST_get_table_entry(header_func_node);
        if (entry == table_entry) {
            return func_node;
        }
    }
    printf("Error: run_fcall_node() could not find AST func node to execute\n");
    exit(1);
}