#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

struct stack {
	int data;
	struct stack* link;
};

void Stack_free(Stack* s) {
	Stack* tmp;
	while (s != NULL) {
		tmp = s;
		s = s->link;
		free(tmp);
	}
}

void Stack_push(int data, Stack** s) {
	Stack* c = (Stack*) malloc(sizeof(Stack));
	c->data = data;
	c->link = *s;
	*s = c;
}

int Stack_pop(Stack** s) {
	/*if (s == NULL || *s == NULL) {
		printf("Error: Pop in empty or invalid stack pointer.\n");
		exit(1);
	}*/
	int data = (*s)->data; 
	Stack* tmp = (*s);
	(*s) = (*s)->link;
	free(tmp);
	return data;
}

int Stack_isEmpty(Stack* s) {
	return (s == NULL ? 1 : 0);
}

void Stack_print(Stack* s) {
	if (s == NULL) {
		printf(" Stack is empty!\n");
		return;
	}
	//printf("\n Stack:\n");
	Stack* c = s;
	do {
		printf("|\t%d\t|\n",c->data);
		c = c->link;
	} while (c != NULL);
}
