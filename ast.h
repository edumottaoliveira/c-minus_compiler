#ifndef AST_H
#define AST_H

typedef enum {
	ID_NODE,
	NUM_NODE,
	FUNC_LIST_NODE,
	FUNC_DECL_NODE,
	FUNC_HEADER_NODE,
	FUNC_BODY_NODE,
	PARAM_LIST_NODE,
	VAR_LIST_NODE,
	BLOCK_NODE,
	ASSIGN_NODE,
	IF_NODE,
	WHILE_NODE,
	RETURN_NODE,
	INPUT_NODE,
	OUTPUT_NODE,
	WRITE_NODE,
	FCALL_NODE,
	ARG_LIST_NODE,
	BOOL_LT_NODE,
	BOOL_LE_NODE,
	BOOL_GT_NODE,
	BOOL_GE_NODE,
	BOOL_EQ_NODE,
	BOOL_NEQ_NODE,
	PLUS_NODE,
	MINUS_NODE,
	TIMES_NODE,
	OVER_NODE,
	FUNC_NAME_NODE,
	STRING_NODE,
	VAR_DECL_NODE,
	VAR_USE_NODE
} NodeKind;

struct node; // Opaque structure to ensure encapsulation.

typedef struct node AST;

AST* AST_new_leaf(NodeKind kind, int data, void* tableEntry);

AST* AST_new_node(NodeKind kind, int numChildren, ...);

void AST_add_child(AST* parent, AST* child);

int AST_count_children(AST* parent);

NodeKind AST_get_kind(AST* node);

int AST_get_data(AST* node);

void AST_set_table_entry(AST* node, void* tableEntry);

void* AST_get_table_entry(AST* node);

AST* AST_get_child(AST* node, int index);

void AST_free_tree(AST *tree);

void AST_print_node_info(AST* node);

int AST_print_node_dot(AST* node);

void AST_print_dot(AST *tree);

#endif
