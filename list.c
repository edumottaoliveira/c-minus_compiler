#include <stdio.h>
#include <stdlib.h>
#include "list.h"
	
struct list {
	int value;
	int* array;
	struct list* next;
};

List* List_create() {
	return NULL;
}

int List_appendInt(int value, List** ls) {
	List* c = (List*) malloc(sizeof(List));
	c->value = value;
	c->array = NULL;
	c->next = NULL;

	List* cell = (*ls);
	if (cell == NULL) {
		(*ls) = c;
		return 0;
	}

	int count = 0;
	List* last_cell;
	do {
		last_cell = cell;
		cell = cell->next;
		count++;
	} while (cell != NULL);

	last_cell->next = c;
	return count;
}

int List_appendArray(int* array, List** ls) {
	List* c = (List*) malloc(sizeof(List));
	c->value = 0;
	c->array = array;
	c->next = NULL;

	List* cell = (*ls);
	if (cell == NULL) {
		(*ls) = c;
		return 0;
	}

	int count = 0;
	List* last_cell;
	do {
		last_cell = cell;
		cell = cell->next;
		count++;
	} while (cell != NULL);
	
	last_cell->next = c;
	return count;
}

List* List_getCell(int index, List* ls) {
	int count = 0;
	List* cell = ls;
	while (cell != NULL && count != index) {
		cell = cell->next;
		count++;
	}
	return cell;
}

int List_isArrayCell(List* cell) {
	return (cell->array != NULL);
}

int List_isIntCell(List* cell) {
	return (cell->array == NULL);
}

int List_getValueCell(List* cell) {
	return cell->value;
}

int* List_getArrayCell(List* cell) {
	return cell->array;
}

void List_setValueCell(int value, List* cell) {
	cell->value = value;
}

void List_free(List* ls) {
	List* tmp;
	List* cell = ls;
	while (cell != NULL) {
		tmp = cell;
		cell = cell->next;
		if (tmp->array != NULL)
			free(tmp->array);
		free(tmp);
	}
}

void List_print(List* ls) {
	if (ls == NULL) {
		printf("[]\n");
		return;
	}
	
	List* cell = ls;
	printf("[%d", cell->value);
	cell = cell->next;
	while (cell != NULL) {
		if (List_isIntCell(cell)) {
			printf(", %d", cell->value);
		}
		else {
			printf(", array");
		}
		cell = cell->next;
	}
	printf("]\n");
}


