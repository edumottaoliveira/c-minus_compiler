
#ifndef STACK_H
#define STACK_H

struct stack;
typedef struct stack Stack;

void Stack_free(Stack* s);

void Stack_push(int data, Stack** s);

int Stack_pop(Stack** s);

int Stack_isEmpty(Stack* s);

void Stack_print(Stack* s);

#endif
