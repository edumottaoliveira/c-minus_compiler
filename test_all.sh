PATH_IN="./in"
PATH_OUT="./out"
IN_EXT="cm"
OUT_EXT="out"

shopt -s nullglob # avoid unmatched patterns expand as result values
cd ${PATH_IN}
declare -a FILELIST=(*.$IN_EXT)
cd ..

# get length of the array
listLen=${#FILELIST[@]}

echo $listLen "input files"
printf "\n"

for (( i=0; i<${listLen}; i++ ));
do
  FILENAME=${FILELIST[$i]%.*}
  echo "testing" $FILENAME ":"
  ./trab4 < ${PATH_IN}/$FILENAME.$IN_EXT | diff ${PATH_OUT}/$FILENAME.$OUT_EXT - #> /dev/null
  #./parser < ${PATH_IN}/$FILENAME.$IN_EXT >$PATH_OUT/$FILENAME.$OUT_EXT
  #./parser < ./in/${FILENAME}.cm > ${PATH_OUT}/${FILENAME}.dot
  #dot -Tpdf ${PATH_OUT}/${FILENAME}.dot -o ${PATH_OUT}/${FILENAME}.pdf
  #printf "\n\n"
done
