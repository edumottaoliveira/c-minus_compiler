
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "tables.h"

// Literals Table
// ----------------------------------------------------------------------------

struct lit_table_entry {
    char* literal;
    struct lit_table_entry* next;
};

struct lit_table {
    struct lit_table_entry* first;
    struct lit_table_entry* last;
};

LitTable* LT_create_lit_table() {
    LitTable *lt = malloc(sizeof * lt);
    lt->first = NULL;
    lt->last = NULL;
    return lt;
}

LitTableEntry* LT_add_literal(LitTable* lt, char* s) {

    LitTableEntry* entry = lt->first;
    while (entry != NULL) {
        if (strcmp(entry->literal,s) == 0) {
            return entry;
        }
        entry = entry->next;
    }

    LitTableEntry* newEntry = malloc(sizeof * newEntry);
    int sLen = strlen(s);
    char* literal = malloc( (1 + sLen) * sizeof(char));
    strcpy(literal, s);
    literal[sLen] = '\0';
    newEntry->literal = literal;
    newEntry->next = NULL;

    if (lt->first == NULL) {
        lt->first = newEntry;
        lt->last = newEntry;
    } 
    else {
        lt->last->next = newEntry;
        lt->last = newEntry;
    }
    return newEntry;
}

char* LT_get_literal(LitTableEntry* lte) {
    return lte->literal;
}

void LT_print_lit_table(LitTable* lt) {
    printf("Literals table:\n");
    LitTableEntry* entry = lt->first;
    int count = 0;
    while (entry != NULL) {
         printf("Entry %d -- %s\n", count, entry->literal);
         count++;
         entry = entry->next;
    }
    printf("\n");
}

void LT_free_lit_table(LitTable* lt) {
    LitTableEntry* tmp;
    LitTableEntry* entry = lt->first;

    while (entry != NULL) {
        free(entry->literal);
        tmp = entry;
        entry = entry->next;
        free(tmp);
    }
    free(lt);
}

// Symbols Table
// ----------------------------------------------------------------------------

struct entry_sym_table {
  char* name;
  int line;
  int arity;
  int scope;
  int offset;
  struct entry_sym_table* next;
};

struct sym_table {
    struct entry_sym_table* first;
    struct entry_sym_table* last;
};

SymTable* ST_create_sym_table() {
    SymTable *st = malloc(sizeof * st);
    st->first = NULL;
    st->last = NULL;
    return st;
}

SymTableEntry* ST_lookup_var(SymTable* st, char* s, int scope) {
    SymTableEntry* entry = st->first;
    while (entry != NULL) {
        if (entry->scope == scope && strcmp(entry->name,s) == 0) {
            return entry;
        }
        entry = entry->next;
    }
    return NULL;
}

SymTableEntry* ST_lookup_func(SymTable* st, char* s, int arity) {
    SymTableEntry* entry = st->first;
    while (entry != NULL) {
        if (strcmp(entry->name,s) == 0) {
            return entry;
        }
        entry = entry->next;
    }
    return NULL;
}

SymTableEntry* ST_add_var(SymTable* st, char* s, int line, int scope) {

    SymTableEntry* newEntry = malloc(sizeof * newEntry);
    int sLen = strlen(s);
    char* name = malloc( (1 + sLen) * sizeof(char));
    strcpy(name, s);
    name[sLen] = '\0';
    newEntry->name = name;
    newEntry->line = line;
    newEntry->scope = scope;
    newEntry->arity = -1;
    newEntry->next = NULL;

    if (st->first == NULL) {
        st->first = newEntry;
        st->last = newEntry;
    } 
    else {
        st->last->next = newEntry;
        st->last = newEntry;
    }
    return newEntry;
}

SymTableEntry* ST_add_func(SymTable* st, char* s, int line, int arity) {

    SymTableEntry* newEntry = malloc(sizeof * newEntry);
    int sLen = strlen(s);
    char* name = malloc( (1 + sLen) * sizeof(char));
    strcpy(name, s);
    name[sLen] = '\0';
    newEntry->name = name;
    newEntry->line = line;
    newEntry->scope = -1;
    newEntry->arity = arity;
    newEntry->next = NULL;

    if (st->first == NULL) {
        st->first = newEntry;
        st->last = newEntry;
    } 
    else {
        st->last->next = newEntry;
        st->last = newEntry;
    }
    return newEntry;
}

char* ST_get_name(SymTableEntry* entry) {
    return entry->name;
}

int ST_get_line(SymTableEntry* entry) {
    if (entry == NULL) {
        printf("aaaaa");
    }
    return entry->line;
}

int ST_get_arity(SymTableEntry* entry) {
    return entry->arity;
}

int ST_get_scope(SymTableEntry* entry) {
    return entry->scope;
}

int ST_get_offset(SymTableEntry* entry) {
    return entry->offset;
}

void ST_set_offset(SymTableEntry* entry, int offset) {
    if (entry == NULL) {
        printf("Error: offset with offset (%d) to NULL entry\n", offset);
        exit(1);
    }
    entry->offset = offset;
}

void ST_print_var_table(SymTable* st) {
    printf("variables table:\n");
    int count;
    SymTableEntry* entry = st->first;
    count = 0;
    while (entry != NULL) {
        printf("Entry %d -- name: %s, line: %d, scope: %d", 
        count, ST_get_name(entry), ST_get_line(entry), ST_get_scope(entry));
        printf("\n");
        count++;
        entry = entry->next;
    }    
    printf("\n");
}

void ST_print_var_table_entry(SymTableEntry* entry) {
    if (entry == NULL) {
        printf("Error: ST_print_var_table_entry(SymTableEntry* entry) with NULL entry\n");
        return;
    }
    printf("SymTableEntry -- name: %s, line: %d, arity: %d, offset: %d\n", 
        ST_get_name(entry), ST_get_line(entry), ST_get_arity(entry), ST_get_offset(entry));
}

void ST_print_func_table(SymTable* st) {
    printf("functions table:\n");
    int count;
    SymTableEntry* entry = st->first;
    count = 0;
    while (entry != NULL) {
        printf("Entry %d -- name: %s, line: %d, arity: %d", 
        count, ST_get_name(entry), ST_get_line(entry), ST_get_arity(entry));
        printf("\n");
        count++;
        entry = entry->next;
    }    
    printf("\n");
}

void ST_free_entries(SymTableEntry* firstEntry) {
    SymTableEntry* tmp;
    SymTableEntry* entry = firstEntry;

    while (entry != NULL) {
        free(entry->name);
        tmp = entry;
        entry = entry->next;
        free(tmp);
    }
}

void ST_free_sym_table(SymTable* st) {
  if (st != NULL) {
    ST_free_entries(st->first);
    st->first = NULL;
    st->last = NULL;
    free(st);
  }
}
