#include <stdio.h>
#include <stdlib.h>
#include "stack.h"
#include "list.h"
#include "frame.h"

Frame* frame_ptr = NULL;

struct frame {
    List* local; // local variable list
    Stack* stack; // int stack
    struct frame* ctrl_link; // pointer to last frame
};

Frame* Frame_create(List* local, Stack* stack) {
    Frame* f = (Frame*) malloc (sizeof(Frame));
    f->local = local;
    f->stack = stack;
    f->ctrl_link = NULL;
    return f;
}

void Frame_pushFrame(Frame* f) {
    f->ctrl_link = frame_ptr;
    frame_ptr = f;
}

void Frame_popFrame() {
	if (frame_ptr == NULL) {
		printf("Error: Frame_popFrame on null frame_ptr\n");
		return;
	}
    Frame* tmp = frame_ptr;
    frame_ptr = frame_ptr->ctrl_link;
    Frame_free(tmp);
}

void Frame_free(Frame* f) {
	if (f == NULL) {
        printf("Warning: Frame_free on null frame_ptr\n");
		return;
	}
    List_free(f->local);
    Stack_free(f->stack);
    free(f);
}

int Frame_appendLocalInt(int value, Frame* f) {
    return (List_appendInt(value, &(f->local)));
}

int Frame_appendLocalArray(int* array, Frame* f) {
    return (List_appendArray(array, &(f->local)));
}

int Frame_getValueLocalInt(int offset, Frame* f) {
    List* cell = List_getCell(offset, f->local);
    return (List_getValueCell(cell));
}

void Frame_setValueLocalInt(int value, int offset, Frame* f){
    List* cell = List_getCell(offset, f->local);
    List_setValueCell(value, cell);
}

void Frame_stackPushNum(int value, Frame* f) {
    Stack_push(value, &(f->stack));
}

void Frame_stackPushLocal(int offset, Frame* f) {
    List* cell = List_getCell(offset, f->local);
    int value = List_getValueCell(cell);
    Stack_push(value, &(f->stack));
}

int Frame_stackPop(Frame* f) {
	if (f == NULL) {
		printf("Error: Frame_stackPop on null frame\n");
		exit(1);
	}
    return (Stack_pop( &(f->stack)));
}

int Frame_isStackEmpty(Frame* f) {
    if (f == NULL) {
		printf("Error: Frame_isStackEmpty on null frame\n");
		exit(1);
	}
    return (Stack_isEmpty(f->stack));
}

void Frame_print(Frame *f) {
	if (f == NULL) {
		printf("Error: Frame_print on null frame\n");
		return;
	}
    printf("::Frame structure::\n");
    printf("\n- Local variables:\n ");
    List_print(f->local);
    printf("\n- Stack: \n");
    Stack_print(f->stack);
    printf("\n");
}

